#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <clocale>
#include <locale>
#include <fstream>
#include <ostream>
#include <unordered_set>
#include <cinttypes>
#include <stdint.h>
#ifndef TEXTSEARCH_H
#define TEXTSEARCH_H

class TIndex {
public:

    bool GetName(std::wifstream& input);

    void PrintNames();
    void IndexAll(std::string& inputName);
    void Print();
    void SaveNames(std::string& outputName);
    void SaveIndex(std::string& outputName);


private:

    inline void Index(uint32_t n, std::wifstream& input);


    std::vector<std::wstring> names;
    std::unordered_map<std::wstring, std::vector<uint32_t> > index;

};

#endif
